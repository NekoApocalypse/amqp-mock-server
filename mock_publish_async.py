import functools
import logging
import json
import pika
from pika.exchange_type import ExchangeType

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)


class MockDataPublisher():
    EXCHANGE = 'regularity-amqp-test'
    EXCHANGE_TYPE = ExchangeType.direct
    QUEUE = 'regularity.input.data'
    ROUTING_KEY = QUEUE

    # connection callbacks
    def on_connection_open(self, _connection):
        LOGGER.info('Connection opened')
        self.open_channel()

    def on_connection_open_error(self, _connection, err):
        LOGGER.error('Connection open failed, reopening in 5 seconds: %s', err)
        self._connection.ioloop.call_later(5, self._connection.ioloop.stop)

    def on_connection_closed(self, _connection, reason):
        self._channel = None
        if self._stopping:
            self._connection.ioloop.stop()
        else:
            LOGGER.warning('Connection closed, reopening in 5 seconds: %s',
                           reason)
            self._connection.ioloop.call_later(5, self._connection.ioloop.stop)

    # channel callbacks
    def on_channel_open(self, channel):

        def start_work(_frame, callback):
            callback()

        def setup_binding(_frame, callback):
            LOGGER.info(
                f"Setting up binding: {self.EXCHANGE} -> {self.QUEUE}, key: {self.ROUTING_KEY}"
            )
            self._channel.queue_bind(
                queue=self.QUEUE,
                exchange=self.EXCHANGE,
                routing_key=self.ROUTING_KEY,
                callback=callback,
            )

        def setup_queue(_frame, callback):
            LOGGER.info(f"Setting up queue {self.QUEUE}")
            self._channel.queue_declare(
                queue=self.QUEUE,
                callback=callback,
            )

        def declare_exchange(_frame, callback):
            LOGGER.info(f"Declaring exchange {self.EXCHANGE}")
            self._channel.exchange_declare(
                exchange=self.EXCHANGE,
                exchange_type=self.EXCHANGE_TYPE,
                callback=callback,
            )

        LOGGER.info("Channel opened")
        self._channel = channel
        self._channel.add_on_close_callback(self.on_channel_closed)

        on_binding_ok = functools.partial(
            start_work,
            callback=self.schedule_work,
        )

        on_queue_declare_ok = functools.partial(
            setup_binding,
            callback=on_binding_ok,
        )
        on_exchange_declare_ok = functools.partial(
            setup_queue,
            callback=on_queue_declare_ok,
        )
        declare_exchange(None, on_exchange_declare_ok)

    def on_channel_closed(self, channel, reason):
        LOGGER.warning(f"Channel {channel} was closed: {reason}")
        if not self._stopping:
            self._connection.close()

    def schedule_work(self):
        # self._connection.ioloop.add_callback(self.work)
        self._connection.ioloop.call_later(0, self.work)

    def work(self):
        if self._channel is None or not self._channel.is_open:
            return
        #
        self._channel.basic_publish(
            self.EXCHANGE,
            self.ROUTING_KEY,
            body=f"test {self._message_number}",
        )
        self._message_number += 1
        #
        self.schedule_work()

    # helder methods
    def open_channel(self):
        LOGGER.info("Creating a new channel")
        self._connection.channel(on_open_callback=self.on_channel_open)

    def connect(self):
        LOGGER.info(f"Connected to {self._url}")
        return pika.SelectConnection(
            pika.URLParameters(self._url),
            on_open_callback=self.on_connection_open,
            on_open_error_callback=self.on_connection_open_error,
            on_close_callback=self.on_connection_closed,
        )

    def __init__(self, amqp_url):
        self._connection: pika.SelectConnection = None
        self._channel = None

        self._deliveries = None
        self._acked = None
        self._nacked = None
        self._message_number = None

        self._stopping = False
        self._url = amqp_url

    def run(self):
        while not self._stopping:
            self._connection = None
            self._deliveries = []
            self._acked = 0
            self._nacked = 0
            self._message_number = 0

            try:
                self._connection = self.connect()
                self._connection.ioloop.start()
            except KeyboardInterrupt:
                self.stop()
                if (self._connection is not None
                        and not self._connection.is_closed):
                    # Finish closing
                    self._connection.ioloop.start()

        LOGGER.info('Stopped')

    def stop(self):
        LOGGER.info('Stopping')
        self._stopping = True
        self.close_channel()
        self.close_connection()

    def close_channel(self):
        if self._channel is not None:
            LOGGER.info('Closing the channel')
            self._channel.close()

    def close_connection(self):
        if self._connection is not None:
            LOGGER.info('Closing connection')
            self._connection.close()


def main():
    logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)
    mock_publisher = MockDataPublisher('amqp://guest:guest@localhost:5672/')
    mock_publisher.run()


if __name__ == "__main__":
    main()
