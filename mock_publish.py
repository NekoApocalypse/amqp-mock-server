import pika

EXCHANGE = "regularity-amqp-test"
QUEUE = "regularity.input.data"


def amqp_connect():
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.queue_declare(queue=QUEUE)
    channel.exchange_declare(EXCHANGE)
    channel.queue_bind(QUEUE, EXCHANGE)
    return connection, channel


if __name__ == "__main__":
    connection, channel = amqp_connect()
    total_count = 1000000
    report_count = 100000
    for i in range(total_count):
        if (i % report_count) == 0:
            print(f"Message Count: {i} of {total_count}")
        channel.basic_publish(
            exchange=EXCHANGE,
            routing_key=QUEUE,
            body=f"test {i}",
        )
