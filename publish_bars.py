import pika
import polars as pl
import regularity_bars_pb2


DATA_FILE = "./origin/10y_day_price.parquet"
EXCHANGE = "regularity-amqp-test"
QUEUE_INPUT = "regularity.input.data"

def read_data():
    # read data file
    df = pl.read_parquet(DATA_FILE)
    price_df = df.filter(pl.col("freq") == "1d").select([
        pl.col("datetime"),
        pl.col("symbol"),
        pl.col("freq"),
        pl.col("open"),
        pl.col("high"),
        pl.col("low"),
        pl.col("last"),
        pl.col("vol"),
    ])
    return price_df


def amqp_connect():
    """
    Connect to RabbitMQ and check if queue exists
    """
    # setup connection
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    # check if queue exists
    try:
        channel.queue_declare(queue=QUEUE_INPUT, auto_delete=True, passive=True)
    except ValueError as err:
        connection.close()
        raise err
    channel.exchange_declare(EXCHANGE)
    channel.queue_bind(QUEUE_INPUT, EXCHANGE)
    return connection, channel


def row_to_message(message, row):
        message.head = regularity_bars_pb2.Bar.Head.DATA
        # time is in Unix Timestamp (ms)
        message.body.time = int(row[0].timestamp() * 1000)
        message.body.symbol = row[1]
        message.body.freq = row[2]
        message.body.values[:] = [
            row[3],
            row[4],
            row[5],
            row[6],
            row[7],
        ]
        return message


def publish_message(channel, message):
    # print(message)
    channel.basic_publish(
        exchange=EXCHANGE,
        routing_key=QUEUE_INPUT,
        body=message.SerializeToString(),
        # property=pika.BasicProperties(
        #     content_type='text/plain',
        #     delivery_mode=pika.DeliveryMode.Transient,
        # ),
    )


def publish_data(channel, df: pl.DataFrame):
    """
    Publish df data to channel
    """
    # send data
    message = regularity_bars_pb2.Bar()
    for i in range(df.height):
    # for i in range(3):
        row = df.row(i)
        message = row_to_message(message, row)
        # print(row)
        if channel is not None:
            publish_message(channel, message)
        if i % 100000 == 0:
            print(f"Rows Count: {i} of {df.height}")
    # send term signal
    message = regularity_bars_pb2.Bar()
    message.head = regularity_bars_pb2.Bar.Head.TERM
    if channel is not None:
        publish_message(channel, message)

if __name__ == "__main__":
    #  read data
    df = read_data()
    print(df)
    
    # connect to rabbit mq
    connection, channel = amqp_connect()

    publish_data(channel, df)
    
    # close connection
    connection.close()
