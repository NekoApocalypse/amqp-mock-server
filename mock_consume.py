import pika

import sys, os

EXCHANGE = ""
QUEUE = "hello"


def amqp_connect():
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.queue_declare(queue=QUEUE)
    if EXCHANGE != "":
        channel.exchange_declare(EXCHANGE)
        channel.queue_bind(QUEUE, EXCHANGE)
    return connection, channel


def message_callback_mock(ch, method_frame, properties, body):
    print(body)
    channel.basic_ack(delivery_tag=method_frame.delivery_tag)
    return


if __name__ == "__main__":
    connection, channel = amqp_connect()
    channel.basic_consume(queue=QUEUE,
                          on_message_callback=message_callback_mock)
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        print("Interrupted")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
